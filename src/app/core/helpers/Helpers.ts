import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Helpers {
  private authenticationChanged = new Subject<boolean>();

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
  }

  public isAuthenticated(): boolean {
    return (
                isPlatformBrowser(this.platformId) &&
                !(window.localStorage['token'] === undefined ||
                window.localStorage['token'] === null ||
                window.localStorage['token'] === 'null' ||
                window.localStorage['token'] === 'undefined' ||
                window.localStorage['token'] === ''));
  }

  public isAuthenticationChanged(): any {
      return this.authenticationChanged.asObservable();
  }

  public getToken(): any {
    if (
      isPlatformBrowser(this.platformId) &&
      (
        window.localStorage['token'] === undefined ||
        window.localStorage['token'] === null ||
        window.localStorage['token'] === 'null' ||
        window.localStorage['token'] === 'undefined' ||
        window.localStorage['token'] === ''
      )
      ) {

      return '';
    }

    const obj = JSON.parse(window.localStorage['token']);
    return obj.token;
  }

  public setToken(data: any): void {
    this.setStorageToken(JSON.stringify(data));
  }

  public failToken(): void {
    this.setStorageToken(undefined);
  }

  public logout(): void {
    this.setStorageToken(undefined);
  }

  private setStorageToken(value: any): void {
    window.localStorage['token'] = value;
    this.authenticationChanged.next(this.isAuthenticated());
  }
}
