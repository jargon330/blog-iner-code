export interface ICard {
  key: string;
  active: boolean;
  title: string;
  description1: string;
  description2?: string;
  technos?: string;
  path: string;
  url: string;
  img: string;
  alt: string;
}
