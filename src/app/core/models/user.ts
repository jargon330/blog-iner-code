import { Location } from './location';

export interface User {
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  picture: string;
  title: string;

  gender?: 'male' | 'female' | 'other' | '';
  dateOfBirth?: string;
  registerDate?: string;
  phone?: string;
  location?: Location;
}
