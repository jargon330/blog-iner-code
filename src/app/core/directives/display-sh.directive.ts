import { Directive, ElementRef, HostListener, Input, Renderer2, OnChanges, SimpleChanges, OnInit } from '@angular/core';

@Directive({
  selector: '[appDisplaySh]'
})
export class DisplayShDirective implements OnInit, OnChanges {

  @Input('appDisplaySh') display: boolean;

  constructor(
    private element: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
console.log('display='+this.display)

  }

  ngOnChanges(changes: SimpleChanges): void {

    if (this.display) {
      this.element.nativeElement.style.display = 'inline';
    } else {
      this.element.nativeElement.style.display = 'none';
    }
    console.log(changes.display.currentValue)
  }


}
