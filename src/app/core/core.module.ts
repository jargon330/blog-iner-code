import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialElevationDirective } from './directives/material-elevation.directive';
import { ButtonNavigateComponent } from './components/button-navigate/button-navigate.component';
import { MaterialModule } from '../shared/material/material.module';
import { DisplayShDirective } from './directives/display-sh.directive';



@NgModule({
  declarations: [MaterialElevationDirective, ButtonNavigateComponent, DisplayShDirective,  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [MaterialElevationDirective, ButtonNavigateComponent, DisplayShDirective, ],
})
export class CoreModule { }
