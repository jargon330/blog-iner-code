import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-button-navigate',
  templateUrl: './button-navigate.component.html',
  styleUrls: ['./button-navigate.component.scss']
})
export class ButtonNavigateComponent implements OnInit {
  @Input() path: string;
  @Input() disabled: boolean;

  constructor(private router: Router) {}

  ngOnInit(): void {
  }

  navigate(): void {
    this.router.navigate([this.path]);
  }
}
