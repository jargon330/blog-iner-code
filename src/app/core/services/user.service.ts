import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Helpers } from '../helpers/Helpers';
import { environment } from '../../../environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  private baseUrlApi: string = environment.urlApiUser;
  private appId: string = environment.appId;

  constructor(private http: HttpClient, helper: Helpers) {
    super(helper);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrlApi + '/api/user?limit=10', super.header(this.appId));
  }
}
