import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { Helpers } from '../helpers/Helpers';
import { environment } from '../../../environments/environment';
import { Product } from '../models/product';

// https://fakestoreapi.com/

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService {

  private baseUrlApi: string = environment.urlApiProduct;
  private appId: string = environment.appId;

  constructor(private http: HttpClient, helper: Helpers) {
    super(helper);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrlApi + '/products?limit=256', super.header(this.appId));
  }
}
