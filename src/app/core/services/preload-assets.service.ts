import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Product } from '../models/product';
import { ProductService } from './product.service';
import { LoaderService } from '../../core/services/loader.service';

@Injectable({
  providedIn: 'root'
})
export class PreloadAssetsService {

  imgList: Array<string> = ['default.jpg', 'angular.jpg', 'api_platform.jpg', 'poo_php.jpg', 'symfony.jpg', 'twig.jpg', ];
  productsSubject = new ReplaySubject<Product[]>(1);

  constructor(
    private productService: ProductService,
    private loaderService: LoaderService,
  ) { }

  preload(): void {
    //  profil
    this.preloadImage('assets/images/profil/dukoid.jpg');

    // images de la page: home
    this.preloadImage('assets/images/home/technos_default.png');
    this.preloadImage('assets/images/home/technos.jpg');

    // images de la page: mes-sites
    this.preloadImage('assets/images/mes-sites/ecran_quiz_default.jpg');
    this.preloadImage('assets/images/mes-sites/ecran_quiz.jpg');

    // images de la page: display products
    this.preloadImage('assets/images/product/default.jpg');

    //  images de la page: tutos
    for (const imgStr of this.imgList) {
      this.preloadImage('assets/images/technos/' + imgStr);
    }
  }

  preloadImage(img: string): void {
    new Image().src = img;
  }

  getProductsSubject(): ReplaySubject<Product[]> {
    return this.productsSubject;
  }

  productsNext(): void {
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.loaderService.hide();
      if (products) {
        this.productsSubject.next(products);
      }
    });
  }
}
