import { Inject, Injectable, Injector, PLATFORM_ID } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { isPlatformServer } from '@angular/common';
import { LoaderService } from '../services/loader.service';
import { PreloadAssetsService } from '../services/preload-assets.service';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  private cache = new Map<string, any>();

  constructor(
    private readonly injector: Injector,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    private loaderService: LoaderService,
    private preloadAssetsService: PreloadAssetsService
    ) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    this.loaderService.show();
    //
    const isServer = isPlatformServer(this.platformId);
    // console.log('------------ isServer='+isServer)

    if (!req.url.includes('/products')) {
      this.loaderService.hide();
      return next.handle(req);
    }

    if (req.method !== 'GET') {
      this.loaderService.hide();
      return next.handle(req);
    }

    const cachedResponse = this.cache.get(req.url);
    if (cachedResponse) {
      this.loaderService.hide();
      return of(cachedResponse);
    }

    return next.handle(req).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {

          //  preload des assets
          //this.preloadAssetsService.preload();

          //  preload des données de l'api
          this.loaderService.hide();
          this.cache.set(req.url, event);
        }
      })
    );
  }
}
