import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProductsListComponent } from './products-list.component';
import { MaterialModule } from '../../shared/material/material.module';
import { CardProductModule } from '../card-product/card-product.module';



@NgModule({
  declarations: [ProductsListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    CardProductModule,

  ],
  exports: [ProductsListComponent],
})
export class ProductsListModule { }
