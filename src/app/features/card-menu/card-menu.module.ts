import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardMenuRefComponent } from './card-menu-ref/card-menu-ref.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [CardMenuRefComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LazyLoadImageModule,
  ],
  exports: [CardMenuRefComponent],
})
export class CardMenuModule { }
