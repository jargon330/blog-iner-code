import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardMenuRefComponent } from './card-menu-ref.component';

describe('CardMenuRefComponent', () => {
  let component: CardMenuRefComponent;
  let fixture: ComponentFixture<CardMenuRefComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CardMenuRefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMenuRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
