import { Component, Input, OnInit } from '@angular/core';
import { ICard } from '../../../core/models/i-card';

@Component({
  selector: 'app-card-menu-ref',
  templateUrl: './card-menu-ref.component.html',
  styleUrls: ['./card-menu-ref.component.scss']
})
export class CardMenuRefComponent implements OnInit {
  @Input() card: ICard;
  // matRipple
  centered = true;
  disabled = false;
  unbounded = false;
  radius = 128;
  color: string;

  constructor() { }

  ngOnInit(): void {
  }

}
