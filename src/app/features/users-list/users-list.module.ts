import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CardUserModule } from '../card-user/card-user.module';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [UsersListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    CardUserModule,
  ],
  exports: [UsersListComponent],
})
export class UsersListModule { }
