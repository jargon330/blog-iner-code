import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardProductRefComponent } from './card-product-ref/card-product-ref.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [CardProductRefComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LazyLoadImageModule,
  ],
  exports: [CardProductRefComponent],
})
export class CardProductModule { }
