import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../core/models/product';

@Component({
  selector: 'app-card-product-ref',
  templateUrl: './card-product-ref.component.html',
  styleUrls: ['./card-product-ref.component.scss']
})
export class CardProductRefComponent implements OnInit {
  @Input() product: Product;

  constructor() { }

  ngOnInit(): void {
  }

}
