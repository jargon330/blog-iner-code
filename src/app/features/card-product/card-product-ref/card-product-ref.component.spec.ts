import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardProductRefComponent } from './card-product-ref.component';

describe('CardProductRefComponent', () => {
  let component: CardProductRefComponent;
  let fixture: ComponentFixture<CardProductRefComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CardProductRefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardProductRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
