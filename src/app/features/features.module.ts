import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardMenuModule } from './card-menu/card-menu.module';
import { CardUserModule } from './card-user/card-user.module';
import { UsersListModule } from './users-list/users-list.module';
import { ProductsListModule } from './products-list/products-list.module';
import { CardProductModule } from './card-product/card-product.module';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CardMenuModule,
    CardUserModule,
    UsersListModule,
    ProductsListModule,
    CardProductModule,

  ]
})
export class FeaturesModule { }
