import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardUserRefComponent } from './card-user-ref/card-user-ref.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [CardUserRefComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LazyLoadImageModule,
  ],
  exports: [CardUserRefComponent],
})
export class CardUserModule { }
