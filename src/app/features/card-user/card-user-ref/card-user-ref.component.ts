import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../core/models/user';

@Component({
  selector: 'app-card-user-ref',
  templateUrl: './card-user-ref.component.html',
  styleUrls: ['./card-user-ref.component.scss']
})
export class CardUserRefComponent implements OnInit {
  @Input() user: User;

  constructor() { }

  ngOnInit(): void {

  }

}
