import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardUserRefComponent } from './card-user-ref.component';

describe('CardUserRefComponent', () => {
  let component: CardUserRefComponent;
  let fixture: ComponentFixture<CardUserRefComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CardUserRefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardUserRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
