import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ProductService } from './core/services/product.service';
import { Product } from './core/models/product';
import { Observable } from 'rxjs';
import { Subscription, timer } from 'rxjs';
import { PreloadAssetsService } from './core/services/preload-assets.service';
import { LoaderService } from './core/services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  products: Product[];
  productsSubscription: Subscription;

  constructor(
    private title: Title,
    private meta: Meta,
    private productService: ProductService,
    private preloadAssetsService: PreloadAssetsService,
    private loaderService: LoaderService,
  ) {

  }

  ngOnInit(): void {
      // SEO metadata
      this.title.setTitle('iner code');
      this.meta.addTag({name: 'description', content: 'site web développé en Angular 10 et divers tutoriaux sur les frameworks Angular, Symfony'});

      // Twitter metadata
      this.meta.addTag({name: 'twitter:card', content: 'summary'});
      this.meta.addTag({name: 'twitter:site', content: '@AngularUniv'});
      this.meta.addTag({name: 'twitter:title', content: 'site web en Angular 10'});
      this.meta.addTag({name: 'twitter:description', content: 'site web développé en Angular 10 et divers tutoriaux sur les frameworks Angular, Symfony'});
      this.meta.addTag({name: 'twitter:text:description', content: 'site web développé en Angular 10 et divers tutoriaux sur les frameworks Angular, Symfony'});
      this.meta.addTag({name: 'twitter:image', content: 'https://avatars3.githubusercontent.com/u/16628445?v=3&s=200'});
  }

  ngAfterViewInit(): void {

    this.productsSubscription = this.preloadAssetsService.getProductsSubject().subscribe((products: Product[]) => {
      this.products = products;
    });


    /*
    //
    const source = timer(400);
    source.subscribe(() => {
      this.loaderService.show();
      // déclenchement du chargement des images de toutes les pages
      this.preloadAssetsService.preload();
      this.preloadAssetsService.productsNext();
    });
    */
  }

  ngOnDestroy(): void {
    this.productsSubscription.unsubscribe();
  }
}
