import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material/material.module';
import { FeaturesModule } from './features/features.module';
import { CoreModule } from './core/core.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule} from '@angular/common/http';

import { PagesModule } from './pages/pages.module';
import { httpInterceptorProviders } from './core/interceptors/index';
import { CommonModule } from '@angular/common';
import { ScullyLibModule } from '@scullyio/ng-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'blog-iner-code' }),
    //ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    //ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production, registrationStrategy: 'registerImmediately'}),
    CommonModule,

    AppRoutingModule,
    BrowserAnimationsModule,
    FeaturesModule,
    CoreModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    PagesModule,
    ScullyLibModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
