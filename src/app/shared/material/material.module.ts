import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//
import { MatGridListModule } from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatRippleModule} from '@angular/material/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    //
    MatGridListModule,
    MatCardModule,
    MatRippleModule,
    MatMenuModule,
    MatButtonModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    MatGridListModule,
    MatCardModule,
    MatRippleModule,
    MatMenuModule,
    MatButtonModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
  ]
})
export class MaterialModule { }
