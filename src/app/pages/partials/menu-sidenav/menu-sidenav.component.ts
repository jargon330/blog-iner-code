import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderService } from '../../../core/services/loader.service';

@Component({
  selector: 'app-menu-sidenav',
  templateUrl: './menu-sidenav.component.html',
  styleUrls: ['./menu-sidenav.component.scss']
})
export class MenuSidenavComponent implements OnInit {

  isLoading: Subject<boolean> = this.loaderService.isLoading;
  loader = false;

  constructor(private loaderService: LoaderService, private cdr: ChangeDetectorRef) {

    this.isLoading.subscribe((res: boolean) => {
      if (res || !res) {
        this.loader = res;
        this.cdr.detectChanges();
      }
    });
  }

  ngOnInit(): void {
  }

}
