import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuSidenavComponent } from './menu-sidenav/menu-sidenav.component';
import { MaterialModule } from '../../shared/material/material.module';
import { CoreModule } from '../../core/core.module';


@NgModule({
  declarations: [MenuSidenavComponent],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,

  ],
  exports: [MenuSidenavComponent],
})
export class PartialsModule { }
