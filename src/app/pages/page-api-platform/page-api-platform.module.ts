import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageApiPlatformComponent } from './page-api-platform.component';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PageApiPlatformRoutingModule } from './page-api-platform-routing.module';


@NgModule({
  declarations: [PageApiPlatformComponent],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    PageApiPlatformRoutingModule,

  ],
  exports: [PageApiPlatformComponent],
})
export class PageApiPlatformModule { }
