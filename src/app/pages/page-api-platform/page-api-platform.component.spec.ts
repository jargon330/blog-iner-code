import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageApiPlatformComponent } from './page-api-platform.component';

describe('PageApiPlatformComponent', () => {
  let component: PageApiPlatformComponent;
  let fixture: ComponentFixture<PageApiPlatformComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageApiPlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageApiPlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
