import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageApiPlatformComponent } from './page-api-platform.component';

const routes: Routes = [
  { path: '', component: PageApiPlatformComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageApiPlatformRoutingModule { }
