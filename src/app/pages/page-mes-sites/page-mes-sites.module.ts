import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageMesSitesComponent } from './page-mes-sites.component';
import { PageMesSitesRoutingModule } from './page-mes-sites-routing.module';
import { CoreModule } from 'src/app/core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [PageMesSitesComponent],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    MaterialModule,
    FlexLayoutModule,
    PageMesSitesRoutingModule,
    LazyLoadImageModule,
  ],
  exports: [PageMesSitesComponent],
})
export class PageMesSitesModule { }
