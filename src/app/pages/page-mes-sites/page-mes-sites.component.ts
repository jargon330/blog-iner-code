import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-mes-sites',
  templateUrl: './page-mes-sites.component.html',
  styleUrls: ['./page-mes-sites.component.scss']
})
export class PageMesSitesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClick(path: string): void {
    if (path === 'quiz') {
      window.open('https://quiz-manager.eventoo.fr/', '_blank');
    }
  }
}
