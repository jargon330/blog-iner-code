import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageMesSitesComponent } from './page-mes-sites.component';

describe('PageMesSitesComponent', () => {
  let component: PageMesSitesComponent;
  let fixture: ComponentFixture<PageMesSitesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMesSitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMesSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
