import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageMesSitesComponent } from './page-mes-sites.component';

const routes: Routes = [
  { path: '', component: PageMesSitesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageMesSitesRoutingModule { }
