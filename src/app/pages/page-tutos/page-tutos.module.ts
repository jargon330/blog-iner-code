import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTutosComponent } from './page-tutos.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PartialsModule } from '../partials/partials.module';
import { PageTutosRoutingModule } from './page-tutos-routing.module';
import { CardMenuModule } from 'src/app/features/card-menu/card-menu.module';



@NgModule({
  declarations: [PageTutosComponent, ],
  imports: [
    CommonModule,

    CardMenuModule,
    FlexLayoutModule,
    PartialsModule,
    PageTutosRoutingModule,

  ],
  exports: [PageTutosComponent],
})
export class PageTutosModule { }
