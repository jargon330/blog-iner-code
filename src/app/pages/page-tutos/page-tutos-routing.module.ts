import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageTutosComponent } from './page-tutos.component';

const routes: Routes = [
  { path: '', component: PageTutosComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageTutosRoutingModule { }
