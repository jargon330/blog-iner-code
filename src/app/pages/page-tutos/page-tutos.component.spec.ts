import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageTutosComponent } from './page-tutos.component';

describe('PageTutosComponent', () => {
  let component: PageTutosComponent;
  let fixture: ComponentFixture<PageTutosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
