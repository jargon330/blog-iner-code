import { OnInit, Component, AfterViewChecked, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import menu from '../../../assets/config/menu.json';
import { ICard } from '../../core/models/i-card';
import { BaseComponent } from '../base/base.component';


@Component({
  selector: 'app-page-tutos',
  templateUrl: './page-tutos.component.html',
  styleUrls: ['./page-tutos.component.scss']
})
export class PageTutosComponent extends BaseComponent implements OnInit, AfterViewInit {
  menuData: any = menu;
  //
  show = true;

  constructor(
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
  }

  onClick(card: ICard): void {
    if (null !== card.path) {
      this.router.navigate([card.path]);
    } else if (null !== card.url) {
      window.open(card.url, '_blank');
    }
  }

  ngAfterViewInit(): void {
    super.preloadImagesTemplate();
  }
}
