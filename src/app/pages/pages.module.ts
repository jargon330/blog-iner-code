import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTutosModule } from './page-tutos/page-tutos.module';
import { PageHomeModule } from './page-home/page-home.module';
import { CardMenuModule } from '../features/card-menu/card-menu.module';
import { PageAngularModule } from './page-angular/page-angular.module';
import { PagePooPhpModule } from './page-poo-php/page-poo-php.module';
import { PageApiPlatformModule } from './page-api-platform/page-api-platform.module';
import { PageSymfonyLibModule } from './page-symfony-lib/page-symfony-lib.module';
import { PageSymfonyListenerModule } from './page-symfony-listener/page-symfony-listener.module';
import { BaseComponent } from './base/base.component';
import { PageDisplayProductsModule } from './page-display-products/page-display-products.module';
import { PageMesSitesModule } from './page-mes-sites/page-mes-sites.module';


@NgModule({
  declarations: [BaseComponent],
  imports: [
    CommonModule,
    CardMenuModule,
    PageTutosModule,
    PageHomeModule,
    PageAngularModule,
    PagePooPhpModule,
    PageApiPlatformModule,
    PageSymfonyLibModule,
    PageSymfonyListenerModule,
    PageDisplayProductsModule,
    PageMesSitesModule,

  ],
  exports: [

  ],

})
export class PagesModule { }
