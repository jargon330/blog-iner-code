import { Component, OnInit, OnDestroy } from '@angular/core';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { Product } from '../../core/models/product';
import { Subscription } from 'rxjs';
import { PreloadAssetsService } from '../../core/services/preload-assets.service';

@Component({
  selector: 'app-page-display-products',
  templateUrl: './page-display-products.component.html',
  styleUrls: ['./page-display-products.component.scss']
})
export class PageDisplayProductsComponent extends BaseComponent implements OnInit, OnDestroy {

  productsSubscription: Subscription;
  products: Product[];
  enable = false;
  //
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string,
    private preloadAssetsService: PreloadAssetsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.productsSubscription = this.preloadAssetsService.getProductsSubject().subscribe((products: Product[]) => {
      this.products = products;
      this.enable = true;
    });
  }

  ngOnDestroy(): void {
    this.productsSubscription.unsubscribe();
  }
}
