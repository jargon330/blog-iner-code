import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageDisplayProductsComponent } from './page-display-products.component';

describe('PageDisplayProductsComponent', () => {
  let component: PageDisplayProductsComponent;
  let fixture: ComponentFixture<PageDisplayProductsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDisplayProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDisplayProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
