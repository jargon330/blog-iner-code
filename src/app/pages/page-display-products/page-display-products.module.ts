import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDisplayProductsRoutingModule } from './page-display-products-routing.module';
import { PageDisplayProductsComponent } from './page-display-products.component';
import { CoreModule } from '@angular/flex-layout';
import { PartialsModule } from '../partials/partials.module';
import { ProductsListModule } from 'src/app/features/products-list/products-list.module';


@NgModule({
  declarations: [PageDisplayProductsComponent],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    PageDisplayProductsRoutingModule,
    ProductsListModule,
  ],
  exports: [PageDisplayProductsComponent],
})
export class PageDisplayProductsModule { }
