import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageDisplayProductsComponent } from './page-display-products.component';

const routes: Routes = [
  {
    path: '',
    component: PageDisplayProductsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageDisplayProductsRoutingModule { }
