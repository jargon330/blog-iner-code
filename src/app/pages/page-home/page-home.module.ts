import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHomeComponent } from './page-home.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PageHomeRoutingModule } from './page-home-routing.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [PageHomeComponent, ],
  imports: [
    CommonModule,
    MaterialModule,
    CoreModule,
    PartialsModule,
    PageHomeRoutingModule,
    LazyLoadImageModule,
  ],
  exports: [PageHomeComponent],
})
export class PageHomeModule { }
