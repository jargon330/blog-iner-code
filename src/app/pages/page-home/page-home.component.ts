import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { PreloadAssetsService } from '../../core/services/preload-assets.service';
import { Subscription, timer } from 'rxjs';
import { Product } from 'src/app/core/models/product';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent extends BaseComponent implements OnInit, AfterViewInit {

  products: Product[];

  constructor(
    private preloadAssetsService: PreloadAssetsService
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngAfterViewInit(): void {

  }
}
