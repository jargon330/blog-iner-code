import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageAngularComponent } from './page-angular.component';

describe('PageAngularComponent', () => {
  let component: PageAngularComponent;
  let fixture: ComponentFixture<PageAngularComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAngularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAngularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
