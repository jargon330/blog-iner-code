import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-page-angular',
  templateUrl: './page-angular.component.html',
  styleUrls: ['./page-angular.component.scss']
})
export class PageAngularComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {

  }

  scrollTo(to: string): void {
    const element = document.getElementById(to);
    element.scrollIntoView();
  }
}
