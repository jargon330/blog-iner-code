import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageAngularComponent } from './page-angular.component';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PageAngularRoutingModule } from './page-angular-routing.module';

@NgModule({
  declarations: [PageAngularComponent],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    PageAngularRoutingModule,

  ],
  exports: [PageAngularComponent],
})
export class PageAngularModule { }
