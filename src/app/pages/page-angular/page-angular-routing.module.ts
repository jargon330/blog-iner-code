import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageAngularComponent } from './page-angular.component';

const routes: Routes = [
  { path: '', component: PageAngularComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageAngularRoutingModule { }
