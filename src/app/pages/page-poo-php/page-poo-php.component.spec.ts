import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PagePooPhpComponent } from './page-poo-php.component';

describe('PagePooPhpComponent', () => {
  let component: PagePooPhpComponent;
  let fixture: ComponentFixture<PagePooPhpComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePooPhpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePooPhpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
