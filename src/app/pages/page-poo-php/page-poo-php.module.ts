import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagePooPhpComponent } from './page-poo-php.component';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PagePooPhpRoutingModule } from './page-poo-php-routing.module';

@NgModule({
  declarations: [PagePooPhpComponent, ],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    PagePooPhpRoutingModule,
  ],
  exports: [PagePooPhpComponent, ],
})
export class PagePooPhpModule { }
