import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagePooPhpComponent } from './page-poo-php.component';

const routes: Routes = [
  { path: '', component: PagePooPhpComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagePooPhpRoutingModule { }
