import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-poo-php',
  templateUrl: './page-poo-php.component.html',
  styleUrls: ['./page-poo-php.component.scss']
})
export class PagePooPhpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  scroll(id: string): void {
    const element = document.getElementById(id);
    element.scrollIntoView();
  }


}
