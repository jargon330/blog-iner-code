import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit, AfterViewChecked, AfterViewInit {

  constructor() {
  }

  ngOnInit(): void {

  }

  ngAfterViewChecked(): void {

  }

  ngAfterViewInit(): void {

  }

  preloadAll(): void {

  }

  preloadImagesTemplate(): void {

  }
}
