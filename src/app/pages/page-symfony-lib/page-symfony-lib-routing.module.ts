import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageSymfonyLibComponent } from './page-symfony-lib.component';

const routes: Routes = [
  { path: '', component: PageSymfonyLibComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageSymfonyLibRoutingModule { }
