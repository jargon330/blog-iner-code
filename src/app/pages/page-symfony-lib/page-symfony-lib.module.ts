import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageSymfonyLibComponent } from './page-symfony-lib.component';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PageSymfonyLibRoutingModule } from './page-symfony-lib-routing.module';



@NgModule({
  declarations: [PageSymfonyLibComponent],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    PageSymfonyLibRoutingModule,
  ],
  exports: [PageSymfonyLibComponent],
})
export class PageSymfonyLibModule { }
