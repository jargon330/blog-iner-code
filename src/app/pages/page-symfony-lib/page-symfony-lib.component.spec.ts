import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageSymfonyLibComponent } from './page-symfony-lib.component';

describe('PageSymfonyLibComponent', () => {
  let component: PageSymfonyLibComponent;
  let fixture: ComponentFixture<PageSymfonyLibComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSymfonyLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSymfonyLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
