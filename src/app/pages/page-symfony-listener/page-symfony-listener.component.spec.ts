import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PageSymfonyListenerComponent } from './page-symfony-listener.component';

describe('PageSymfonyListenerComponent', () => {
  let component: PageSymfonyListenerComponent;
  let fixture: ComponentFixture<PageSymfonyListenerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSymfonyListenerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSymfonyListenerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
