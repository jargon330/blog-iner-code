import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageSymfonyListenerComponent } from './page-symfony-listener.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CoreModule } from '../../core/core.module';
import { PartialsModule } from '../partials/partials.module';
import { PageSymfonyListenerRoutingModule } from './page-symfony-listener-routing.module';



@NgModule({
  declarations: [PageSymfonyListenerComponent],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    PartialsModule,
    PageSymfonyListenerRoutingModule,
  ],
  exports: [PageSymfonyListenerComponent],
})
export class PageSymfonyListenerModule { }
