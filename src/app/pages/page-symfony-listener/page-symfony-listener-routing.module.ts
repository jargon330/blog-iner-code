import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageSymfonyListenerComponent } from './page-symfony-listener.component';

const routes: Routes = [
  { path: '', component: PageSymfonyListenerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageSymfonyListenerRoutingModule { }
