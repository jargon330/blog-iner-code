import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreloadAllModules } from '@angular/router';
import { PageAngularModule } from './pages/page-angular/page-angular.module';
import { PageApiPlatformModule } from './pages/page-api-platform/page-api-platform.module';
import { PageDisplayProductsModule } from './pages/page-display-products/page-display-products.module';
import { PageHomeModule } from './pages/page-home/page-home.module';
import { PageMesSitesModule } from './pages/page-mes-sites/page-mes-sites.module';
import { PagePooPhpModule } from './pages/page-poo-php/page-poo-php.module';
import { PageSymfonyLibModule } from './pages/page-symfony-lib/page-symfony-lib.module';
import { PageSymfonyListenerModule } from './pages/page-symfony-listener/page-symfony-listener.module';
import { PageTutosModule } from './pages/page-tutos/page-tutos.module';
import { UserResolverService } from './pages/resolvers/user-resolver.service';

/*
  { path: 'home', loadChildren: () => import('./pages/page-home/page-home.module').then(m => m.PageHomeModule), data: { preload: true } },
  { path: 'tutos', loadChildren: () => import('./pages/page-tutos/page-tutos.module').then(m => m.PageTutosModule), data: { preload: true } },
  { path: 'angular', loadChildren: () => import('./pages/page-angular/page-angular.module').then(m => m.PageAngularModule), data: { preload: true } },
  { path: 'poo-php', loadChildren: () => import('./pages/page-poo-php/page-poo-php.module').then(m => m.PagePooPhpModule), data: { preload: true } },
  { path: 'api-platform', loadChildren: () => import('./pages/page-api-platform/page-api-platform.module').then(m => m.PageApiPlatformModule), data: { preload: true } },
  { path: 'symfony-lib', loadChildren: () => import('./pages/page-symfony-lib/page-symfony-lib.module').then(m => m.PageSymfonyLibModule), data: { preload: true } },
  { path: 'symfony-listener', loadChildren: () => import('./pages/page-symfony-listener/page-symfony-listener.module').then(m => m.PageSymfonyListenerModule), data: { preload: true } },
  { path: 'display-products', loadChildren: () => import('./pages/page-display-products/page-display-products.module').then(m => m.PageDisplayProductsModule), data: { preload: true } },
  { path: 'mes-sites', loadChildren: () => import('./pages/page-mes-sites/page-mes-sites.module').then(m => m.PageMesSitesModule), data: { preload: true } },

  { path: 'home', component: PageHomeModule },
  { path: 'tutos', component: PageTutosModule },
  { path: 'angular', component: PageAngularModule },
  { path: 'poo-php', component: PagePooPhpModule },
  { path: 'api-platform', component: PageApiPlatformModule },
  { path: 'symfony-lib', component: PageSymfonyLibModule },
  { path: 'symfony-listener', component: PageSymfonyListenerModule },
  { path: 'display-products', component: PageDisplayProductsModule },
  { path: 'mes-sites', component: PageMesSitesModule },  
*/

const routes: Routes = [

  { path: 'home', component: PageHomeModule },
  { path: 'tutos', component: PageTutosModule },
  { path: 'angular', component: PageAngularModule },
  { path: 'poo-php', component: PagePooPhpModule },
  { path: 'api-platform', component: PageApiPlatformModule },
  { path: 'symfony-lib', component: PageSymfonyLibModule },
  { path: 'symfony-listener', component: PageSymfonyListenerModule },
  { path: 'display-products', component: PageDisplayProductsModule },
  { path: 'mes-sites', component: PageMesSitesModule },  

  { path: '',   redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  //imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, initialNavigation: 'enabled', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
