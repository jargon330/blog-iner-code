import { ScullyConfig } from '@scullyio/scully';
export const config: ScullyConfig = {
  projectRoot: "./src",
  projectName: "blog-iner-code",
  outDir: './dist/static',
  routes: {
  }
};